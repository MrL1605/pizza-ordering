package co.mrl.pizzaordering;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * Created by lalit on 22/9/15.
 */

public class PaymentActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_activity);

        SharedPreferences sp = getSharedPreferences("Pizza Prefs", MODE_PRIVATE);
        Intent intent = getIntent();
        int total_cost = intent.getExtras().getInt("total_cost", 0);
        String u_name = sp.getString("cust_name", "");
        TextView temp_tv = (TextView) findViewById(R.id.cost_tv);
        temp_tv.setText(u_name + ", Your Bill costs :  Rs." + total_cost);
        temp_tv = (TextView)findViewById(R.id.thanking_tv);
        temp_tv.setText("Thank You for your purchase, " + u_name);
        findViewById(R.id.cancel_bt).setOnClickListener(this);
        findViewById(R.id.pay_bt).setOnClickListener(this);
        findViewById(R.id.order_again_bt).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.order_again_bt) {
            startActivity(new Intent(PaymentActivity.this, PizzaChoiceActivity.class));
            finish();
        } else if (v.getId() == R.id.cancel_bt) {
            startActivity(new Intent(PaymentActivity.this, PizzaChoiceActivity.class));
            finish();
        } else if (v.getId() == R.id.pay_bt) {
            findViewById(R.id.pay_ll).setVisibility(View.GONE);
            findViewById(R.id.payed_ll).setVisibility(View.VISIBLE);
        }

    }
}
