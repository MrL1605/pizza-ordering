package co.mrl.pizzaordering;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomerLoginActivity extends Activity {

    EditText email_et, name_et;
    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private Pattern pattern;
    private Matcher matcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_login);

        email_et = (EditText) findViewById(R.id.email_et);
        name_et = (EditText) findViewById(R.id.name_et);

        findViewById(R.id.login_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email_str = email_et.getText().toString();
                String name_str = name_et.getText().toString();

                if (name_str.length() < 3) {
                    Toast.makeText(CustomerLoginActivity.this,
                            "Enter Valid Name",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                pattern = Pattern.compile(EMAIL_PATTERN);
                matcher = pattern.matcher(email_str);
                if (!matcher.matches()) {
                    Toast.makeText(CustomerLoginActivity.this,
                            "Enter Valid Email",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                SharedPreferences sp = getSharedPreferences("Pizza Prefs", MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.clear();
                editor.putString("cust_name", name_str);
                editor.putString("cust_email", email_str);
                editor.apply();
                startActivity(new Intent(CustomerLoginActivity.this, PizzaChoiceActivity.class));
                finish();

            }
        });


    }


}
