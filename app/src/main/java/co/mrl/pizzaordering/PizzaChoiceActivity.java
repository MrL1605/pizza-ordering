package co.mrl.pizzaordering;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by lalit on 22/9/15.
 */
public class PizzaChoiceActivity extends Activity {

    String[] PIZZA_NAME_LIST = {
            "piz1",
            "piz2",
            "piz3",
            "piz4"
    };

    Integer[] PIZZA_COST_LIST = {
            250,
            200,
            300,
            500
    };

    String[] TOPPINGS = {
            "extras1",
            "extras2",
    };
    Integer[] quantity = new Integer[PIZZA_NAME_LIST.length];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pizza_choice_activity);

        LinearLayout ll = (LinearLayout) findViewById(R.id.pizza_list_ll);

        for (int index = 0; index < PIZZA_NAME_LIST.length; index++) {

            quantity[index] = 0;
            final int curr_index = index;
            String pizza_name = PIZZA_NAME_LIST[index];
            int pizza_cost = PIZZA_COST_LIST[index];
            LinearLayout pizza_ll = new LinearLayout(this);
            LinearLayout.LayoutParams wrap_params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            LinearLayout.LayoutParams button_params = new LinearLayout.LayoutParams(
                    55,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            pizza_ll.setLayoutParams(wrap_params);
            TextView pizza_title_tv = new TextView(this);
            pizza_title_tv.setLayoutParams(wrap_params);
            final TextView quantity_counter = new TextView(this);
            quantity_counter.setLayoutParams(wrap_params);
            quantity_counter.setText("0");
            Button minus_one = new Button(this);
            Button plus_one = new Button(this);
            minus_one.setLayoutParams(button_params);
            plus_one.setLayoutParams(button_params);
            minus_one.setText("-");
            plus_one.setText("+");
            minus_one.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (quantity[curr_index] > 0) {
                        quantity_counter.setText("" + (quantity[curr_index] - 1));
                        quantity[curr_index]--;
                    }
                }
            });
            plus_one.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    quantity_counter.setText("" + (quantity[curr_index] + 1));
                    quantity[curr_index]++;
                }
            });

            pizza_title_tv.setText(pizza_name + "(Rs. " + pizza_cost + ")");
            pizza_title_tv.setTextSize(18f);
            pizza_ll.addView(pizza_title_tv);
            pizza_ll.addView(minus_one);
            pizza_ll.addView(quantity_counter);
            pizza_ll.addView(plus_one);
            ll.addView(pizza_ll);

        }

        LinearLayout extras_ll = (LinearLayout)findViewById(R.id.extras_ll);
        LinearLayout.LayoutParams match_wrap = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        for (String ext_name : TOPPINGS){
            CheckBox ext_cb = new CheckBox(this);
            ext_cb.setLayoutParams(match_wrap);
            ext_cb.setText(ext_name);
            extras_ll.addView(ext_cb);
        }

        CheckBox extras_cb = (CheckBox) findViewById(R.id.extras_cb);
        extras_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) findViewById(R.id.extras_ll).setVisibility(View.VISIBLE);
                else findViewById(R.id.extras_ll).setVisibility(View.GONE);
            }
        });

        findViewById(R.id.payment_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int total_cost = 0;
                for (int index = 0; index < PIZZA_NAME_LIST.length; index++) {
                    Log.i("List:", PIZZA_NAME_LIST[index] + " " + PIZZA_COST_LIST[index] + " " + quantity[index]);
                    total_cost += (quantity[index] * PIZZA_COST_LIST[index]);
                }
                Log.i("Cost:", total_cost + "");
                Intent intent = new Intent(PizzaChoiceActivity.this, PaymentActivity.class);
                intent.putExtra("total_cost",total_cost);
                startActivity(intent);
                finish();
            }
        });

    }
}
